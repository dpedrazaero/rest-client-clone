module.exports = function MetaData() {
  const Json = {
    "_meta": {
        "class": "Metadata"
    },
    // "version": "8.4.0-SNAPSHOT-${build.number}",
    // "environment": "MADRID",
    "services": {
      "booking": "OPERATIONAL",
      "pnr": "OPERATIONAL",
      "shopping": "OPERATIONAL"
    },
    "_links": {
      "self": {
          "href": "/"
      },
      "airports": {
          "href": "/airports?store={storeCode}&pos={posCode}"
      },
      "airportsOrigin": {
          "href": "/airports/origin?store={storeCode}&pos={posCode}"
      },
      "airportsArrival": {
          "href": "/airports/arrival/{departureKey}"
      },
      "airportsByLocation": {
          "href": "/airports/byLocation?store={storeCode}&pos={posCode}&countryCode={countryCode}&latitude={latitude}&longitude={longitude}&radius={radius}"
      },
      "bestFares": {
          "href": "/fares/best?city-pair-dates={cityPairDateCodes}&travelers={travelersCode}&store={storeCode}&coupon={couponCode}&discount={discountCode}&pos={posCode}&with-prices={withPrices}"
      },
      "detailedFares": {
          "href": "/fares/detailed?multi-city={multiCity}&city-pair-dates={cityPairDateCodes}&travelers={travelersCode}&store={storeCode}&coupon={couponCode}&discount={discountCode}&cartId={cartId}&pos={posCode}&fareId={fareId}&fareType={fareType}&isSDC={isSDC}&leg={leg}"
      },
      "detailedFaresAmenities": {
          "href": "/fares/detailed/amenities?flightDetails={flightDetails}"
      },
      "formsOfPayment": {
          "href": "/payment-methods?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "shoppingCart": {
          "href": "/carts/{cartId}?store={storeCode}&pos={posCode}&language={language}"
      },
      "shoppingCartCollection": {
          "href": "/carts?store={storeCode}&pos={posCode}"
      },
      "ancillaries": {
          "href": "/ancillaries?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "seatmaps": {
          "href": "/seatmaps?cartId={cartId}&store={storeCode}&pos={posCode}"
      },
      "preselect": {
          "href": "/seatmaps/preSelectSeat?cartId={cartId}&store={storeCode}&pos={posCode}&segmentCode={segmentCode}"
      },
      "fareRules": {
          "href": "/fare-rules?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "bookingDeeplink": {
          "href": "/book?store={storeCode}&pos={posCode}"
      },
      "purchaseOrder": {
          "href": "/po?store={storeCode}&pos={posCode}&language={language}"
      },
      "shareItinerary": {
          "href": "/itinerary/share/?cartId={cartId}&store={store}&language={language}&pnr={pnr}"
      },
      "getItinerary": {
          "href": "/itinerary/share/?store={store}&language={language}&pnr={pnr}&type={type}&showQuote={showQuote}&showPnr={showPnr}"
      },
      "reroutePayment": {
          "href": "/po/3ds-reroute"
      },
      "paypalSession": {
          "href": "/payment-methods/paypal?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "monthlyLowestFares": {
          "href": "/monthly-lowest-fares?store={storeCode}&pos={posCode}&origin={origin}&destination={destination}&months={monthsAhead}"
      },
      "refreshSession": {
          "href": "/carts/refreshSession/{cartId}?store={storeCode}&pos={posCode}&language={language}"
      },
      "subscriptions": {
          "href": "/am-subscriptions-api/subscribe?language={language}&store={storeCode}"
      },
      "unsubscriptions": {
          "href": "/am-subscriptions-api/unsubscribe?pnr={pnr}&email={email}"
      },
      "addFlights": {
          "href": "/am-trips-api/flights/add"
      },
      "flightsListDevice": {
          "href": "/am-trips-api/flights/{deviceId}/list"
      },
      "notificationToDevice": {
          "href": "/am-trips-api/notifications/device"
      },
      "mergeDevice": {
          "href": "/am-trips-api/flights/{deviceId}/merge"
      },
      "mergeFlights": {
          "href": "/am-trips-api/flights/merge"
      },
      "notificationToAncillaries": {
          "href": "/am-trips-api/notifications/ancillaries"
      },
      "unlinkDevice": {
          "href": "/am-trips-api/flights/{deviceId}/unlink"
      },
      "findFlightsFFnumber": {
          "href": "/am-trips-api/flights/list"
      },
      "unlinkEmail": {
          "href": "/am-trips-api/flights/email/unlink"
      },
      "updateEmail": {
          "href": "/am-trips-api/email/update"
      },
      "deleteFlightsByKey": {
          "href": "/am-trips-api/flights/delete/{key}"
      },
      "unlinkPNR": {
          "href": "/am-trips-api/flights/unlink/pnr/{pnr}"
      },
      "unlinkPNRDevice": {
          "href": "/am-trips-api/flights/unlink/pnr/{pnr}/device/{deviceId}"
      },
      "tripCentralDateReport": {
          "href": "/am-trips-api/reports/departureDate/{departureDate}"
      },
      "tripCentralSegmentReport": {
          "href": "/am-trips-api/reports/segment/{segmentCode}"
      },
      "unlinkFFN": {
          "href": "/am-trips-api/flights/unlink/ffn/{ffn}"
      },
      "multyFareSearch": {
          "href": "/multyFareSearch?store={storeCode}&pos={posCode}&departureAirport={departureAirport}&arrivalAirport={arrivalAirport}&outboundDate={outboundDate}&inboundDate={inboundDate}"
      },
      "pnr": {
          "href": "/checkin/pnr?store={storeCode}&pos={posCode}&premiereUpgrade={premiereUpgrade}&language={language}"
      },
      "pnrEnhanced": {
          "href": "/pnr/enhanced?store={storeCode}&pos={posCode}&premiereUpgrade={premiereUpgrade}"
      },
      "checkinFormsOfPayment": {
          "href": "/checkin/payment-methods?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "checkinShoppingCart": {
          "href": "/checkin/carts/{cartId}?store={storeCode}&pos={posCode}&language={language}"
      },
      "checkinAncillaries": {
          "href": "/checkin/ancillaries?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "checkinSeatmaps": {
          "href": "/checkin/seatmaps?cartId={cartId}&store={storeCode}&pos={posCode}"
      },
      "checkinPreselect": {
          "href": "/checkin/seatmaps/preSelectSeat?cartId={cartId}&store={storeCode}&pos={posCode}"
      },
      "checkinPurchaseOrder": {
          "href": "/checkin/po?store={storeCode}&pos={posCode}&language={language}"
      },
      "boardingPass": {
          "href": "/checkin/boarding-passes/{pnr}?store={storeCode}&pos={posCode}&legCode={legCode}&language={language}"
      },
      "flightStatus": {
          "href": "/checkin/flight-status?store={storeCode}&pos={posCode}&flight={flightNumber}&date={departureDate}&origin={origin}&destination={destination}"
      },
      "emailBoardingPass": {
          "href": "/checkin/email-boarding-pass?store={storeCode}&pos={posCode}&email={email}&language={language}&boarding-pass-id={boardingPassId}&travelers={ticketNumbers}&leg={legCode}&cartId={cartId}"
      },
      "emailReceipt": {
          "href": "/checkin/email-receipt?store={storeCode}&pos={posCode}&language={language}&email={email}&pnr={pnr}&transaction={pnr}&cartId={cartId}"
      },
      "upgradeList": {
          "href": "/checkin/passengers/upgradeList?legCode={legCode}&store={storeCode}&pos={posCode}&language={language}"
      },
      "standByList": {
          "href": "/checkin/passengers/standByList?legCode={legCode}&store={storeCode}&pos={posCode}&language={language}"
      },
      "CheckinUpdatePassenger": {
          "href": "/checkin/update-passenger",
          "templated": true
      },
      "CheckinVisaCheckoutParams": {
          "href": "/checkin/am-visa-api/parameters"
      },
      "CheckinVisaCheckoutInfo": {
          "href": "/checkin/am-visa-api/payments/{callId}/resume?cartId={cartId}&promocode={promocode}"
      },
      "CheckinReroutePayment": {
          "href": "/checkin/po/3ds-reroute-ckin/{result3ds}",
          "templated": true
      },
      "manage": {
          "href": "/manage/pnr?store={storeCode}&pos={posCode}&language={language}"
      },
      "manageShoppingCart": {
          "href": "/manage/carts/{cartId}?store={storeCode}&pos={posCode}&language={language}"
      },
      "manageSeatmaps": {
          "href": "/manage/seatmaps?cartId={cartId}&store={storeCode}&pos={posCode}"
      },
      "managePreselect": {
          "href": "/manage/seatmaps/preSelectSeat?cartId={cartId}&store={storeCode}&pos={posCode}"
      },
      "manageAncillaries": {
          "href": "/manage/ancillaries?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "manageFormsOfPayment": {
          "href": "/manage/payment-methods?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "managePurchaseOrder": {
          "href": "/manage/po?store={storeCode}&pos={posCode}&language={language}"
      },
      "manageEmailReceipt": {
          "href": "/manage/email-receipt?store={storeCode}&pos={posCode}&language={language}&email={email}&pnr={pnr}&cartId={cartId}"
      },
      "managePaypalSession": {
          "href": "/manage/payment-methods/paypal?store={storeCode}&cartId={cartId}&pos={posCode}"
      },
      "manageBagBenefit": {
          "href": "/manage/addBagByBenefit/{cartId}?store={storeCode}&pos={posCode}&language={language}"
      },
      "manageMeal": {
          "href": "/manage/meal/menu?segment={segment}&flightNumber={flightNumber}&store={store}"
      },
      "manageSelectMeal": {
          "href": "/manage/meal/select/{cartId}?store={storeCode}&pos={posCode}&language={language}"
      },
      "ManageUpdatePassenger": {
          "href": "/manage/update-passenger",
          "templated": true
      },
      "userProfile": {
          "href": "/user-profile"
      },
      "userProfileEditAccount": {
          "href": "/user-profile-edit-account"
      },
      "userProfileSignUp": {
          "href": "/user-profile-sign-up"
      },
      "userProfileCreateAccount": {
          "href": "/user-profile-createAccount"
      },
      "userProfileSignIn": {
          "href": "/user-profile-sign-in"
      },
      "userProfileSignOut": {
          "href": "/user-profile-sign-out"
      },
      "userProfileEditSettings": {
          "href": "/user-profile-edit-settings"
      },
      "userProfileEditFrequentFlyerProgram": {
          "href": "/user-profile-edit-ffp/{me}?travelerId={travelerId}"
      },
      "userProfilePasswordRecovery": {
          "href": "/user-profile-password-recovery"
      },
      "userProfileIsValidCPSession": {
          "href": "/user-profile-valid-session"
      },
      "userProfileAddTraveler": {
          "href": "/user-profile-add-traveler"
      },
      "userProfileUpdateTraveler": {
          "href": "/user-profile-update-traveler/{nickname}"
      },
      "userProfileRemoveTraveler": {
          "href": "/user-profile-remove-traveler/{nickname}"
      },
      "userProfileAddTravelerList": {
          "href": "/user-profile-add-traveler-list"
      },
      "userProfileAddCreditCard": {
          "href": "/user-profile-add-card"
      },
      "userProfileUpdateCreditCard": {
          "href": "/user-profile-update-card/{cardID}"
      },
      "userProfileRemoveCreditCard": {
          "href": "/user-profile-remove-card/{cardID}"
      },
      "userProfileAddGiftCard": {
          "href": "/user-profile-add-gift-card"
      },
      "userProfileUpdateGiftCard": {
          "href": "/user-profile-update-gift-card/{giftCardId}"
      },
      "userProfileRemoveGiftCard": {
          "href": "/user-profile-remove-gift-card/{giftCardId}"
      },
      "userProfileCobradCard": {
          "href": "/user-profile-cobrand-card"
      },
      "myTripsList": {
          "href": "/user-profile-trips"
      },
      "addTripInProfile": {
          "href": "/add-trip-profile?language={language}&store={storeCode}"
      },
      "getTotalTrips": {
          "href": "/get-trips-counter"
      },
      "deleteTrip": {
          "href": "/delete-trip-profile"
      },
      "locations": {
          "href": "/am-titanium-api/locations/{openText}/list?lang={lang}&limit={limit}"
      },
      "carOptions": {
          "href": "/am-titanium-api/vehicles/available?lang={lang}&driverAge={driverAge}&driverCountryCode={driverCountryCode}&pickUpDateTime={pickUpDateTime}&pickUpLocationCode={pickUpLocationCode}&dropOffDateTime={dropOffDateTime}&dropOffLocationCode={dropOffLocationCode}"
      },
      "carRateDetails": {
          "href": "/am-titanium-api/rates/{rateReference}?lang={lang}"
      },
      "bookCar": {
          "href": "/am-titanium-api/book/{rateReference}?lang={lang}"
      },
      "visaCheckoutParams": {
          "href": "/am-visa-api/parameters"
      },
      "visaCheckoutInfo": {
          "href": "/am-visa-api/payments/{callId}/resume?cartId={cartId}&promocode={promocode}"
      },
      "masterpassParams": {
          "href": "/am-masterpass-api/parameters"
      },
      "masterpassPaymentInfo": {
          "href": "/am-masterpass-api/payment/{transactionID}/data?cartId={cartId}"
      }
    }
  }

  return Json;
}
